﻿using ShopBridge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestShopBridge
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IInventory inventory = new Inventory();

            ////Add new Product

            //ResponseObject response = inventory.AddProduct(new Product
            //{
            //    Name = "RealMe",
            //    CategoryId = 2,
            //    Description = "This is Real me X Smart Phone",
            //    Price = 21000,
            //    Weight = 600,
            //    Units = 1
            //});

            //Console.WriteLine(response.Description);

            ////Update the Product

            //response = inventory.UpdateProduct(new Product
            //{
            //    Id=1,
            //    Name = "RealMe X",
            //    CategoryId = 2,
            //    Description = "This is Real me X Smart Phone",
            //    Price = 22000,
            //    Weight = 800,
            //    Units = 1
            //});

            //Console.WriteLine(response.Description);

            ////Delete Product

            //response = inventory.DeleteProduct(2);

            //Console.WriteLine(response.Description);

            ////Get Products

            //response = inventory.GetProducts();

            //Console.WriteLine("Id    Name    CategoryId    Description    Price    Weight    Units    Total ");
            //foreach (Product product in response.Response)//DataRowCOllections =Collection of DataRow
            //{
            //    Console.WriteLine($"{product.Id}    {product.Name}    {product.CategoryId}    {product.Description}    {product.Price}    {product.Weight}    {product.Units}    {product.Total}");
            //}

            //Get Products By Id

            ResponseObject response = inventory.GetProductById(1);

            Console.WriteLine("Id    Name    CategoryId    Description    Price    Weight    Units    Total ");
            foreach (Product product in response.Response)//DataRowCOllections =Collection of DataRow
            {
                Console.WriteLine($"{product.Id}    {product.Name}    {product.CategoryId}    {product.Description}    {product.Price}    {product.Weight}    {product.Units}    {product.Total}");
            }

            //Get Products By Search Key

            response = inventory.GetProductsBySearchkey("p");

            Console.WriteLine("Id    Name    CategoryId    Description    Price    Weight    Units    Total ");
            foreach (Product product in response.Response)//DataRowCOllections =Collection of DataRow
            {
                Console.WriteLine($"{product.Id}    {product.Name}    {product.CategoryId}    {product.Description}    {product.Price}    {product.Weight}    {product.Units}    {product.Total}");
            }

            Console.ReadLine();
        }
    }
}
