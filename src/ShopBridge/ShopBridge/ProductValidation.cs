﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBridge
{
    internal static class ProductValidation
    {
        public static ResponseObject IsProductValid(Product product)
        {
            ResponseObject responseObject = new ResponseObject();

            if (product is null)
            {
                responseObject.Description = $"Product should not be null;\n";
            }
            if (string.IsNullOrEmpty(product.Name))
            {
                responseObject.Description += $"Product Name should not be null/empty;\n";
            }
            if (string.IsNullOrEmpty(product.Description))
            {
                responseObject.Description += $"Product Description should not be null/empty;\n";
            }
            if (product.CategoryId <= 0)
            {
                responseObject.Description += $"Product Category should be greater than 0;\n";
            }
            else if (product.Weight <= 0)
            {
                responseObject.Description += $"Product Weight should be greater than 0;\n";
            }

            responseObject.IsSuccess = responseObject.Description.Length < 0;

            return responseObject;
        }

        public static ResponseObject IsProductIdValid(int productId)
        {
            ResponseObject responseObject = new ResponseObject();

            if (productId <= 0)
            {
                responseObject.Description = $"ProductId should not be less than 0;\n";
            }

            responseObject.IsSuccess = responseObject.Description.Length < 0;

            return responseObject;
        }

        public static ResponseObject IsSearchKeyValid(string searchKey)
        {
            ResponseObject responseObject = new ResponseObject();

            if (string.IsNullOrEmpty(searchKey))
            {
                responseObject.Description = $"SearchKey should not be null or empty;\n";
            }

            responseObject.IsSuccess = responseObject.Description.Length < 0;

            return responseObject;
        }
    }
}
