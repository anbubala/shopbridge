﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBridge
{
    public class ResponseObject
    {
        public bool IsSuccess { get; set; }

        public string Description { get; set; }

        public dynamic Response { get; set; }
    }
}
