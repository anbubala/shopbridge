﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBridge
{
    public class Inventory : IInventory
    {
        private readonly string _connectionString;

        public Inventory()
        {
            _connectionString = ConfigurationManager.AppSettings["conString"];
        }

        /// <summary>
        /// Add Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns>Returns ResponseObject which contains status, Description and Response of a Method</returns>
        public ResponseObject AddProduct(Product product)
        {
            ResponseObject responseObject = new ResponseObject();

            var validationStatus = ProductValidation.IsProductValid(product);

            if (validationStatus.IsSuccess)
            {
                try
                {
                    string cmdText = "dbo.AddProduct";//Stored Procedure Name

                    using (SqlConnection con = new SqlConnection(_connectionString))
                    {
                        using (SqlCommand sqlCommand = new SqlCommand(cmdText, con))
                        {
                            sqlCommand.CommandType = CommandType.StoredProcedure;

                            sqlCommand.Parameters.AddWithValue("@Name", product.Name);
                            sqlCommand.Parameters.AddWithValue("@CategoryId", product.CategoryId);
                            sqlCommand.Parameters.AddWithValue("@Description", product.Description);
                            sqlCommand.Parameters.AddWithValue("@Price", product.Price);
                            sqlCommand.Parameters.AddWithValue("@Weight", product.Weight);
                            sqlCommand.Parameters.AddWithValue("@Units", product.Units);
                            sqlCommand.Parameters.AddWithValue("@Total", product.Total);

                            con.Open();
                            int affectedRows = sqlCommand.ExecuteNonQuery();//This will execute insert query

                            if (affectedRows > 0)
                            {
                                responseObject.IsSuccess = true;
                                responseObject.Description = "Record Successfully Inserted";
                            }
                            else
                            {
                                responseObject.IsSuccess = false;
                                responseObject.Description = "Unable to Insert Record. Please contact Admin";
                            }
                            con.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseObject.IsSuccess = false;
                    responseObject.Description = $"Unable to Insert Record. Please contact Admin. Exception Details:{ex.Message}";
                }
            }
            else
            {
                return validationStatus;
            }
            return responseObject;
        }

        /// <summary>
        ///Deletes Product by Product Id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns>Returns ResponseObject which contains status, Description and Response of a Method</returns>
        public ResponseObject DeleteProduct(int productId)
        {
            ResponseObject responseObject = new ResponseObject();

            ResponseObject validationStatus= ProductValidation.IsProductIdValid(productId);

            if (validationStatus.IsSuccess)
            {
                try
                {
                    string cmdText = "dbo.DeleteProduct";

                    using (SqlConnection con = new SqlConnection(_connectionString))
                    {
                        using (SqlCommand sqlCommand = new SqlCommand(cmdText, con))
                        {
                            sqlCommand.CommandType = CommandType.StoredProcedure;

                            sqlCommand.Parameters.AddWithValue("@Id", productId);

                            con.Open();
                            var affectedRows = sqlCommand.ExecuteNonQuery();


                            if (affectedRows > 0)
                            {
                                responseObject.IsSuccess = true;
                                responseObject.Description = "Record Successfully Deleted";
                            }
                            else
                            {
                                responseObject.IsSuccess = false;
                                responseObject.Description = "Unable to Delete Record. Please contact Admin";
                            }

                            con.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseObject.IsSuccess = false;
                    responseObject.Description = $"Unable to Delete Record. Please contact Admin. Exception Details:{ex.Message}";
                }
            }
            else
            {
                return validationStatus;
            }
            return responseObject;
        }

        /// <summary>
        /// Gets Product by Product Id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns>Returns ResponseObject which contains status, Description and Response of a Method</returns>
        public ResponseObject GetProductById(int productId)
        {
            ResponseObject responseObject = new ResponseObject();

            ResponseObject validationStatus = ProductValidation.IsProductIdValid(productId);

            if (validationStatus.IsSuccess)
            {
                try
                {
                    string cmdText = "dbo.GetProductById";//inline query

                    using (SqlConnection con = new SqlConnection(_connectionString))
                    {
                        using (SqlCommand sqlCommand = new SqlCommand(cmdText, con))
                        {
                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.Parameters.AddWithValue("@Id", productId);

                            SqlDataAdapter dataAdapter = new SqlDataAdapter();
                            dataAdapter.SelectCommand = sqlCommand;

                            using (DataTable dataTable = new DataTable())
                            {
                                dataAdapter.Fill(dataTable);

                                responseObject.IsSuccess = true;
                                responseObject.Description = "Products successfully fetched.";
                                List<Product> products = new List<Product>();

                                products = (from DataRow row in dataTable.Rows
                                            select new Product()
                                            {
                                                Id = Convert.ToInt32(row["Id"]),
                                                Name = row["Name"].ToString(),
                                                CategoryId = Convert.ToInt32(row["CategoryId"]),
                                                Description = row["Description"].ToString(),
                                                Price = Convert.ToDecimal(row["Price"]),
                                                Weight = Convert.ToDouble(row["Weight"]),
                                                Units = Convert.ToInt32(row["Units"]),
                                            }).ToList();

                                responseObject.Response = products;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseObject.IsSuccess = false;
                    responseObject.Description = $"Unable to Fetch Products. Please contact Admin. Exception Details:{ex.Message}";
                }
            }
            else
            {
                return validationStatus;
            }

            return responseObject;
        }

        /// <summary>
        /// Gets all the Products 
        /// </summary>
        /// <returns>Returns ResponseObject which contains status, Description and Response of a Method</returns>
        public ResponseObject GetProducts()
        {
            ResponseObject responseObject = new ResponseObject();

            try
            {
                string cmdText = "dbo.GetProducts";//inline query

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand(cmdText, con))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        SqlDataAdapter dataAdapter = new SqlDataAdapter();
                        dataAdapter.SelectCommand = sqlCommand;

                        using (DataTable dataTable = new DataTable())
                        {
                            dataAdapter.Fill(dataTable);

                            responseObject.IsSuccess = true;
                            responseObject.Description = "Products successfully fetched.";

                            List<Product> products = new List<Product>();

                            products = (from DataRow row in dataTable.Rows
                                        select new Product()
                                        {
                                            Id = Convert.ToInt32(row["Id"]),
                                            Name = row["Name"].ToString(),
                                            CategoryId = Convert.ToInt32(row["CategoryId"]),
                                            Description = row["Description"].ToString(),
                                            Price = Convert.ToDecimal(row["Price"]),
                                            Weight = Convert.ToDouble(row["Weight"]),
                                            Units = Convert.ToInt32(row["Units"]),
                                        }).ToList();

                            responseObject.Response = products;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                responseObject.IsSuccess = false;
                responseObject.Description = $"Unable to Fetch Products. Please contact Admin. Exception Details:{ex.Message}";
            }

            return responseObject;
        }

        /// <summary>
        /// Get's all the products which contains searchkey 
        /// </summary>
        /// <param name="searchKey"></param>
        /// <returns>Returns ResponseObject which contains status, Description and Response of a Method</returns>
        public ResponseObject GetProductsBySearchkey(string searchKey)
        {
            ResponseObject responseObject = new ResponseObject();

            ResponseObject validationStatus = ProductValidation.IsSearchKeyValid(searchKey);

            if (validationStatus.IsSuccess)
            {
                try
                {
                    string cmdText = "dbo.GetProductsBySearchKey";//inline query

                    using (SqlConnection con = new SqlConnection(_connectionString))
                    {
                        using (SqlCommand sqlCommand = new SqlCommand(cmdText, con))
                        {
                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.Parameters.AddWithValue("@searchKey", searchKey);

                            SqlDataAdapter dataAdapter = new SqlDataAdapter();
                            dataAdapter.SelectCommand = sqlCommand;

                            using (DataTable dataTable = new DataTable())
                            {
                                dataAdapter.Fill(dataTable);

                                responseObject.IsSuccess = true;
                                responseObject.Description = "Products successfully fetched.";
                                List<Product> products = new List<Product>();

                                products = (from DataRow row in dataTable.Rows
                                            select new Product()
                                            {
                                                Id = Convert.ToInt32(row["Id"]),
                                                Name = row["Name"].ToString(),
                                                CategoryId = Convert.ToInt32(row["CategoryId"]),
                                                Description = row["Description"].ToString(),
                                                Price = Convert.ToDecimal(row["Price"]),
                                                Weight = Convert.ToDouble(row["Weight"]),
                                                Units = Convert.ToInt32(row["Units"]),
                                            }).ToList();

                                responseObject.Response = products;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseObject.IsSuccess = false;
                    responseObject.Description = $"Unable to Fetch Products. Please contact Admin. Exception Details:{ex.Message}";
                }
            }
            else
            {
                return validationStatus;
            }

            return responseObject;
        }

        /// <summary>
        /// Updates all the matching products
        /// </summary>
        /// <param name="product"></param>
        /// <returns>Returns ResponseObject which contains status, Description and Response of a Method</returns>
        public ResponseObject UpdateProduct(Product product)
        {
            ResponseObject responseObject = new ResponseObject();

            var validationStatus = ProductValidation.IsProductValid(product);

            if (validationStatus.IsSuccess)
            {
                try
                {
                    string cmdText = "UpdateProduct";

                    using (SqlConnection con = new SqlConnection(_connectionString))//setting the connection details
                    {
                        using (SqlCommand sqlCommand = new SqlCommand(cmdText, con))//instantiationn of SqlCommand Class
                        {
                            sqlCommand.CommandType = CommandType.StoredProcedure;

                            sqlCommand.Parameters.AddWithValue("@Id", product.Id);
                            sqlCommand.Parameters.AddWithValue("@Name", product.Name);
                            sqlCommand.Parameters.AddWithValue("@CategoryId", product.CategoryId);
                            sqlCommand.Parameters.AddWithValue("@Description", product.Description);
                            sqlCommand.Parameters.AddWithValue("@Price", product.Price);
                            sqlCommand.Parameters.AddWithValue("@Weight", product.Weight);
                            sqlCommand.Parameters.AddWithValue("@Units", product.Units);
                            sqlCommand.Parameters.AddWithValue("@Total", product.Total);

                            con.Open();//Open database Connection

                            var affectedRows = sqlCommand.ExecuteNonQuery();//execute the update query and will give u number of affected records count

                            if (affectedRows > 0)
                            {
                                responseObject.IsSuccess = true;
                                responseObject.Description = "Record Successfully Updated";
                            }
                            else
                            {
                                responseObject.IsSuccess = false;
                                responseObject.Description = "Unable to Update Record. Please contact Admin";
                            }

                            con.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    responseObject.IsSuccess = false;
                    responseObject.Description = $"Unable to Update Record. Please contact Admin. Exception Details:{ex.Message}";
                }
            }
            else
            {
                return validationStatus;
            }

            return responseObject;
        }
    }
}
