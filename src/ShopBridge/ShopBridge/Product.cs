﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBridge
{
    public class Product
    {
        public int Id { get; set; }

        public string Name
        {
            get;
            set;
        }

        public int CategoryId
        {
            get;
            set;
        }       

        public string Description
        {
            get;
            set;
        }

        public decimal Price
        {
            get;
            set;
        }

        public double Weight
        {
            get;
            set;
        }

        public int Units
        {
            get;
            set;
        }

        public decimal Total => Units * Price;
    }
}
