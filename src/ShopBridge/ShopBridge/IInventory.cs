﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBridge
{
    public interface IInventory
    {
        ResponseObject AddProduct(Product product);

        ResponseObject UpdateProduct(Product product);

        ResponseObject DeleteProduct(int productId);

        ResponseObject GetProducts();

        ResponseObject GetProductById(int productId);

        ResponseObject GetProductsBySearchkey(string searchKey);
    }
}
